# React Native HasInternet

_Simple JS API for React Native apps that answers whether you are connected to the internet or not. Works on both Android and iOS._

This library previously (in version 4) used to communicate with [DNS service by Cloudfare and APNIC](https://1.1.1.1/), wikipedia.org and ietf.org, but nowadays (version 5) it works via standard Android and iOS APIs for detecting internet presence.

On Android, we use [ConnectivityManager](https://developer.android.com/reference/android/net/ConnectivityManager) and [NetworkCapabilities.NET_CAPABILITY_VALIDATED](https://developer.android.com/reference/android/net/NetworkCapabilities#NET_CAPABILITY_VALIDATED).

On iOS, this library is just a thin wrapper around [Reachability](https://github.com/tonymillion/Reachability).

## Usage

There is only one API, the `isConnected()` function returning a promise.

```js
import HasInternet from 'react-native-has-internet';

HasInternet.isConnected().then((isConnected) => {
  console.log('Do we have internet connection? ' + isConnected ? 'yes!' : 'no');
});
```

## Installation

Supports only React Native 0.60 or higher.

### Android

- Run `npm install react-native-has-internet --save` to install using npm.

- Edit `android/app/src/main/AndroidManifest.xml` and add the annoated lines as below:

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.reactnativeproject">

    <uses-permission android:name="android.permission.INTERNET" />   <!-- <- Add this line -->

    <application
      android:allowBackup="true"
      android:label="@string/app_name"
      android:icon="@mipmap/ic_launcher"
      android:theme="@style/AppTheme">

    ...
</manifest>
```

### iOS

Nothing special to do. It should just work.
