#import "RNHasInternet.h"
#import <arpa/inet.h>
#import "TonyMillion.h"

@implementation RNHasInternet

RCT_EXPORT_MODULE(HasInternetModule)

RCT_EXPORT_METHOD(isConnected:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  TonyMillion* reach = [TonyMillion reachabilityForInternetConnection];
  bool answer = [reach isReachable];
  if (answer) {
    resolve(@YES);
  } else {
    resolve(@NO);
  }
}

@end
