package com.staltz.reactnativehasinternet;

import android.os.Bundle;
import android.os.AsyncTask;
import android.os.Build;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.util.Log;

import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import android.util.Log;

public class HasInternetModule extends ReactContextBaseJavaModule {

    @Override
    public String getName() {
        return "HasInternetModule";
    }

    private static ReactApplicationContext reactContext;

    public HasInternetModule(ReactApplicationContext context) {
        super(context);
	reactContext = context;
    }

    @ReactMethod
    public void isConnected(Promise promise) {
        final ConnectivityManager cm = (ConnectivityManager) reactContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final NetworkCapabilities nc = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (nc != null) {
                    promise.resolve(nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED));
                } else {
                    promise.resolve(false);
                }
            } else {
                final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null && activeNetwork.isConnected()) {
                    promise.resolve(true);
                } else {
                    promise.resolve(false);
                }
            }
        } else {
            promise.resolve(false);
        }
    }
}
